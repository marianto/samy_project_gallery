import { useState, useEffect } from 'react'
import { get } from '../utils/api'
import { PhotoCard } from './PhotoCard'
import InfiniteScroll from 'react-infinite-scroll-component';
import { Spinner } from './Shared/Spinner/Spinner';
import './PhotoGrid.css'

export default function PhotoGrid({ search }) {

  const [photos, setPhotos] = useState([]);
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(1);

  useEffect(() => {
    setLoading(true);
    const searchUrl = search ? "/?query=" + search + "&page=" + page : "/?page=" + page;
    get(searchUrl).then((result) => {
      console.log(result)
      setPhotos((prevPhotos) => prevPhotos.concat(result));
      setLoading(false)
    })
  }, [search, page]);

  return (
    <InfiniteScroll dataLength={photos.length} hasMore={true} next={() => setPage((prevPage) => prevPage + 1)} loader={<Spinner />}>
      <ul className='photoGrid'>
        {photos.map((photo) => (
          <PhotoCard key={photo.id} photo={photo} />
        ))}
      </ul>
    </InfiniteScroll>
  )
}
