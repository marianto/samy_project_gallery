import { useHistory } from "react-router";
import { useState } from 'react'
import { FaSearch } from "react-icons/fa";
import './SearchBar.css'
import logo from '../assets/images/logo.svg'

export default function SearchBar() {

    const [searchText, setSearchText] = useState([])
    const history = useHistory();


    const handleSubmit = (e) => {
        e.preventDefault();
        history.push("/?search=" + searchText);
        setSearchText('')
    }

    return (
        <form className="searchContainer" onSubmit={handleSubmit}>
            <div> <img className="logo"
                src={logo} alt="logo" /></div>
            <div className="searchBox">
                <input className="searchInput"
                    type="text"
                    value={searchText}
                    autoFocus
                    placeholder="You're looking for something?"
                    onChange={(e) => setSearchText(e.target.value)}
                />
                <button className="searchButton" type='submit'>
                    <FaSearch size={15} className="searchButton"></FaSearch>
                </button>
            </div>
        </form>
    )
}
