import React from 'react'
import { useState, useEffect } from 'react'
import { IoIosThumbsUp } from "react-icons/io";
import { MdThumbUpOffAlt } from "react-icons/md";
import { MdEuroSymbol } from "react-icons/md";
import { IoIosRefresh } from "react-icons/io";

import { apiPost } from '../utils/api'
import './PhotoCard.css'

export function PhotoCard({ photo }) {

  const [liked, setLiked] = useState(false);
  const [likeCounter, setLikeCounter] = useState('')

  useEffect(() => {

    apiPost.post('/:id/likes').then(res => {
      const likeData = res.data;
      console.log(likeData);
      setLikeCounter(likeData);

    }).catch((error) => {
      console.log(error);
    })
  }, [])

  function handleLiked() {
    setLiked(true)
    setLikeCounter(likeCounter + 1)
  }



  return (
    <li className="photoCard">
      <div className="photoCard_container">
        <div className='card'>
          <img
            src={photo.main_attachment.small}
            alt={photo.title} className="photoCard_img"
          />
          <div className='inside'><span className='rate'>{photo.price}<MdEuroSymbol></MdEuroSymbol></span></div>
          <div className='card-link'>
            <div className='card-link-likes'>
              {liked ? (<button><IoIosThumbsUp></IoIosThumbsUp></button>) : (<button onClick={() => { handleLiked(photo.id) }}><MdThumbUpOffAlt></MdThumbUpOffAlt></button>)}
              <span>{photo.likes_count}</span>

            </div>
            <div className='card-link-dislikes'><button><IoIosRefresh></IoIosRefresh></button></div>
          </div>
        </div>
        <figcaption className="photoCard_caption">
          <h3>{photo.title}</h3>
          <div className='photoCard_author'><span>by</span><p>{photo.author}</p></div>
        </figcaption>
      </div>
    </li>
  )
}
