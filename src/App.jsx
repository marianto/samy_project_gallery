import React from "react";
import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import LandingPage from "./pages/LandingPage";


function App() {

  return (
    <Router>
      <main>
        <Switch>
          <Route component={LandingPage} path="/" exact />
        </Switch>
      </main>

    </Router>
  );
}

export default App;
