import React from 'react'
import PhotoGrid from '../components/PhotoGrid'
import SearchBar from '../components/Shared/SearchBar/SearchBar';
import { useQuery } from '../hooks/useQuery';

export default function LandingPage() {
  const query = useQuery();
  const search = query.get("search")
  console.log(search)
  return (
    <div>
      <SearchBar />
      <PhotoGrid key={search} search={search} />
    </div>
  )
}





