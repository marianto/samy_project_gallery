import axios from 'axios';
const API = "http://localhost:3100/images"

export function get(path){
    return fetch(API + path, {
        timeout:6000,
        headers: {
            Accept: 'application/json',
            'Content-Type':'application/json',
            'Access-Control-Allow-Origin':'*',
            'Authorization':'Bearer' + localStorage.getItem('token')
            //haciendo esto no tengo que enviar token en cada una de las peticiones sino que lo almaceno en la localStorage en la instancia de mi api//
        }
    }).then((result)=> result.json());
};

export const apiPost = axios.create({
    baseURL: 'http://localhost:3100/images',
    timeout:6000,
    headers: {
        Accept: 'application/json',
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*',
        'Authorization':'Bearer' + localStorage.getItem('token')
    }
});